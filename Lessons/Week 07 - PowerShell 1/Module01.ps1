# Get-Command retieves a list of all system commands
Get-Command

# Can expand by searching for just a verb or noun
Get-Command -verb "get"
Get-Command -noun "service"

# Get-Help can be used to explain a command
Get-Help Get-Command
Get-Help Get-Command -examples
Get-Help Get-Command -detailed
Get-Help Get-Command -full

# Most commands can also be passed a -? parameter to get help
# This produces the same output as: Get-Help Get-Command
Get-Command -?

# Moving around the file tree
# Get-ChildItem lists all items in current path
Get-ChildItem

# Set-Location will change the current path
Set-Location d:\scrap
Set-Location "c:\projects\SQL Server"

# Pipelining - combine CmdLets for powerdirGet-ChildItem | Where-Object { $_.Length -gt 100kb }
Get-ChildItem | Where-Object { $_.Length -gt 10mb }
Get-ChildItem | Where-Object { $_.Length -gt 1gb }
Get-ChildItem | Where-Object { $_.Length -gt 100kb } | Sort-Object Name

# Can break commands up among several lines
# (note pipe must be last char on line)
Get-ChildItem |
    where-object { $_.Length -gt 100kb } |
    Sort-Object Length

### STOPPED HERE
    
# To specify columns in the output and get nice formatting, use Format-Table
Get-ChildItem |
    where-object { $_.Length -gt 100kb } |
    Sort-Object Length |
    Format-Table -Property Name, Length -AutoSize
    
# You can also use Select-Object to retrieve certain properties from an object
Get-ChildItem | Select-Object Name, Length